<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use Image;
use Hash;
use Session;
use Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show user name & avatar
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function profile()
    {
        return view('profile', array('user' => Auth::user()));
    }

    /**
     * Get a validator on profile update.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' =>  'email|unique:users,email,' . Auth::user() ->id,
        ]);
    }


    /**
     * Update avatar
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function update_avatar(Request $request)
    {
        $this->validator($request->all())->validate();

        $user = Auth::user();

        $msg = [];
        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save(public_path('/uploads/avatars/' . $filename));
            $user->avatar = $filename;
            $msg[] = 'Avatar changed.';
        }

        $current_password = $user->password;
        if (mb_strlen($request->input('password')) > 2 && $request->input('password') == $request->input('password_confirmation')) {
            if (Hash::check($request->input('current-password'), $current_password)) {
                $user->password = Hash::make($request->input('password'));
                $msg[] = 'Password changed.';
            } else {
                Session::flash('message', "Please, enter correct password.");
                Session::flash('alert-class', 'alert-danger');
                return view('profile', array('user' => Auth::user()));
            }

        }

        if ($user->name != $request->input('name')) {
            $user->name = $request->input('name');
            $msg[] = 'Name changed.';
        }

        if ($user->email != $request->input('email')) {
            $user->email = $request->input('email');
            $msg[] = 'Email changed.';
        }


        $user->save();
        Session::flash('message', $msg);
        Session::flash('alert-class', 'alert-info');
        return view('profile', array('user' => Auth::user()));

    }
}
