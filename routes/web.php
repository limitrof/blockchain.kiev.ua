<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['isVerified']], function () {
    Route::get('/profile', 'HomeController@profile');
    Route::post('/profile', 'HomeController@update_avatar');
});

Route::group(['prefix' => 'admin', 'middleware' => 'admin', 'namespace' => 'Admin'], function () {
    Route::group(['middleware' => ['IsAdmin']], function () {
        CRUD::resource('user', 'Admin\UserCrudController');
    });
    // Backpack\MenuCRUD
    CRUD::resource('menu-item', 'MenuItemCrudController');
    CRUD::resource('article', 'ArticleCrudController');
    CRUD::resource('category', 'CategoryCrudController');
    CRUD::resource('tag', 'TagCrudController');
});