@extends('layouts.app')

@section('content')
    <div class="container">
        <h2>{{ $user->name }}'s Profile</h2>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if(Session::has('message') && count(Session::get('message'))>0)
            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">

                @foreach(Session::get('message') as $message)
                    {{ $message }}
                @endforeach

             </p>
        @endif

        <img src="/uploads/avatars/{{ $user->avatar }}"
             style="width:50px; height:50px; float:left; border-radius:50%; margin-right:25px;">

        <form enctype="multipart/form-data" action="/profile" method="POST">

            <div class="row">
                <label class="col-sm-4 control-label float-right">Update Profile Image</label>
                <div class="col-sm-8">
                    <div class="form-group">
                        <input type="file" name="avatar">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4 control-label float-right">Name</div>
                <div class="col-sm-8">
                    <div class="form-group">
                        <input type="text" value="{{ $user->name }}" name="name"/>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-4 control-label float-right">email</div>
                <div class="col-sm-8">
                    <div class="form-group">
                        <input type="text" value="{{ $user->email }}" name="email"/>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4 control-label float-right">Current Password</div>
                <div class="col-sm-8">
                    <div class="form-group">
                        <input type="password" class="form-control" id="current-password"
                               name="current-password"
                               placeholder="Password">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4 control-label float-right">New Password</div>
                <div class="col-sm-8">
                    <div class="form-group">
                        <input type="password" class="form-control" id="password" name="password"
                               placeholder="Password">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-4 control-label float-right">Re-enter
                    Password
                </div>
                <div class="col-sm-8">
                    <div class="form-group">
                        <input type="password" class="form-control" id="password_confirmation"
                               name="password_confirmation" placeholder="Re-enter Password">
                    </div>
                </div>
            </div>

            <input type="submit" class="pull-right btn btn-sm btn-primary">
        </form>
    </div>
@endsection